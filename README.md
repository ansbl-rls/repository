# ansible role for append repositories to centos hosts

## настройки

```
repository_repolinks:
  - https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm

repository_repos_map:
  - name: altinity_clickhouse
    baseurl: https://packagecloud.io/altinity/clickhouse/el/7/$basearch
    repo_gpgcheck: true
    gpgcheck: false
    enabled: true
    gpgkey: https://packagecloud.io/altinity/clickhouse/gpgkey
    sslverify: true
    sslcacert: /etc/pki/tls/certs/ca-bundle.crt
```
